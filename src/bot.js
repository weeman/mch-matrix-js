import {
    MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,
    RustSdkCryptoStorageProvider,
} from "matrix-bot-sdk";
import { config } from "./../config.js";

// This will be the URL where clients can reach your homeserver. Note that this might be different
// from where the web/chat interface is hosted. The server must support password registration without
// captcha or terms of service (public servers typically won't work).
const homeserverUrl = config.homeserverUrl;

// Use the access token you got from login or registration above.
const accessToken = config.accessToken;

const storageProvider = new SimpleFsStorageProvider("./data/bot.json"); // or any other IStorageProvider
const cryptoProvider = new RustSdkCryptoStorageProvider("./data/crypto");

// Finally, let's create the client and set it to autojoin rooms. Autojoining is typical of bots to ensure
// they can be easily added to any room.
const client = new MatrixClient(homeserverUrl, accessToken, storageProvider, cryptoProvider);
AutojoinRoomsMixin.setupOnClient(client);

// Before we start the bot, register our command handler
client.on("room.message", handleCommand);

// Now that everything is set up, start the bot. This will start the sync loop and run until killed.
client.start().then(() => console.log("Bot started!"));

// This is the command handler we registered a few lines up
async function handleCommand(roomId, event) {
    // Only handle text messages
    if (event['content']?.['msgtype'] !== 'm.text') return;
    // The bot should not respont to it's own messages
    if (event['sender'] === await client.getUserId()) return;

    await client.sendMessage(roomId, event['content']);
}
